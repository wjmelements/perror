CC=gcc
CFLAGS=-O3 -std=gnu99

.PHONY: all clean install

all: perror
clean:
	rm -f perror perror.o

DESTDIR=/usr/local
mandir=/usr/local/share/man
man1dir=$(mandir)/man1
install_files=$(man1dir)/perror.1.gz  $(DESTDIR)/bin/perror
install: $(install_files)
uninstall:
	rm -f $(install_files)
$(man1dir)/perror.1.gz: man/perror.1.gz | $(man1dir)
	install -m644 $< $@
$(DESTDIR)/bin/perror: perror | $(DESTDIR)/bin
	install -m755 $< $@
$(DESTDIR) $(DESTDIR)/bin $(mandir) $(man1dir):
	mkdir -p $@

%.gz: %
	gzip $*
perror: perror.o
	$(CC) $(CFLAGS) $^ -o $@
%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

