#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
/*
 * Prints the output of perror for errno values given to standard out (stdout)
*/
int main(int argc, char* argv[]) {
    if (argc < 2) {
        fputs("What error did you want to print?\n", stderr);
        exit(-1);
    }

    // make perror print to stdout
    // if these fail, just go with it
    close(2);
    dup2(1,2);

    int i;
    for (i = 1; i < argc; i++) {
        errno = atoi(argv[i]);
        perror(argv[i]);
    }
    return 0;
}
